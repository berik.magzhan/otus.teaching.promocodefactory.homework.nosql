﻿using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;



namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly IMongoCollection<Preference> _pref;
        private readonly IMongoCollection<Customer> _cust;
        public MongoDbInitializer(IOptions<MongoSettings> mongoSettings)
        {
            var mongoDb = new MongoClient(mongoSettings.Value.ConnectionString)
                .GetDatabase(mongoSettings.Value.DatabaseName);
            _pref = mongoDb.GetCollection<Preference>(typeof(Preference).Name.ToString());
            _cust = mongoDb.GetCollection<Customer>(typeof(Customer).Name.ToString());
        }

        public void InitializeDb()
        {
            _pref.DeleteMany(_ => true);
            _pref.InsertMany(FakeDataFactory.Preferences);
            _cust.DeleteMany(_ => true);
            _cust.InsertMany(FakeDataFactory.Customers);

        }
    }
}
