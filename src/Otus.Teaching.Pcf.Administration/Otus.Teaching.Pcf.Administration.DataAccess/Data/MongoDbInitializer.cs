﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.Repositories;
using MongoDB.Driver;
using Microsoft.Extensions.Options;


namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly IMongoCollection<Employee> _admDCEmployee;
        private readonly IMongoCollection<Role> _admDCRole;

        //---------------------------------------------------------------------------------------------------
        public MongoDbInitializer(IOptions<MongoDbSettings> mongoDbSettings)
        {
            var mongoClient = new MongoClient(mongoDbSettings.Value.ConnectionString);
            var dataBase = mongoClient.GetDatabase(mongoDbSettings.Value.DatabaseName);
            
            _admDCEmployee = dataBase.GetCollection<Employee>(typeof(Employee).Name.ToString());
            _admDCRole = dataBase.GetCollection<Role>(typeof(Role).Name.ToString());
        }

        //---------------------------------------------------------------------------------------------------
        public void InitializeDb()
        {
            _admDCEmployee.DeleteMany(_ => true);
            _admDCRole.DeleteMany(_ => true);
            _admDCEmployee.InsertMany(FakeDataFactory.Employees);
            _admDCRole.InsertMany(FakeDataFactory.Roles);
        }
    }
}
